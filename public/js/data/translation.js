let _translations = {
    "en": {
        message: {
            k_about_header: "About",
            k_skill_header: "Skill",
            k_skills_title: "Skills",
            k_about_me: "About me",
            k_experience_header: "Experience",
            k_education_header: "Education",
            k_work_header: "Work",
            k_contact_header: "Contact",
            k_language_header: "Lang",
            k_cv_header: "CV",
            k_cv_language_english_header: "English",
            k_cv_language_german_header: "German"
        }
    },
    "de": {
        message: {
            k_about_header: "Über",
            k_skill_header: "E",
            k_experience_header: "E",
            k_education_header: "E",
            k_work_header: "E",
            k_contact_header: "E",
            k_language_header: "Sprache",
            k_cv_header: "CV",
            k_cv_language_english_header: "Englisch",
            k_cv_language_german_header: "Deutsch"
        }
    }
}

function get_translations()
{
    return _translations;
}

function get_translation(lang)
{
    return _translations[lang];
}