
function _get_default_language(proposed_language)
{
    return (proposed_language in ["en", "de"]) ? proposed_language: "en";
}

function pageReady()
{
    console.log(get_translations());
    var lang = navigator.language;
    var default_language = "en"
    if (lang.split("-").length > 0)
    {
        default_language = _get_default_language(lang.split("-"));
    }
    else if (lang.split("_").length > 0)
    {
        default_language = _get_default_language(lang.split("-"));
    }

    Vue.component("header-button", {
        props: ['text_body', 'data_target'],
        template: '<li><a href="#" data-toggle="modal" v-bind:data-target="data_target" class="w3ls-hover"><span v-bind:data-letters="$t(\'message.\' + text_body)"> {{ $t("message." + text_body) }}</span></a></li>'
    });

    Vue.component("header-button-download", {
        props: ['text_body', 'data_target'],
        template: '<li><a href="cv/english.pdf" target="_blank"  class="w3ls-hover"><span v-bind:data-letters="$t(\'message.\' + text_body)"> {{ $t("message." + text_body) }}</span></a></li>'
    });
    

    var messages = get_translations();
    
    var i18n = new VueI18n({
        locale: default_language,
        messages
    });

    var portfolio = new Vue({
        i18n,
        get_translation: function(key) {
            return $t("message." + key);
        },
        watch: {
        },
        methods: {
            header_data_letters: function (key) {
                return $t("message." + key);
            },
            reload_language: function(lang) {
                i18n.locale = lang;
            }
        }

        // data: {
        //     k_about: $t("message.k_about")
        // },
        // mounted: function () {
        //     $("#about-span-hover").data("data-letters", this._t(this.message.k_about))
        // }
    }).$mount("#portfolio");
}

$(document).ready(pageReady);